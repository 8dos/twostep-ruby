.PHONY: test release upload
test:
	rake

release:
	gem build twostep.gemspec

upload:
	gem push twostep-0.5.2.gem	