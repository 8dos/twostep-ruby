module Twostep
  class TokenStore

    # Load token from store 
    def load
      # To be overridden by subclass
      nil
    end

    # Save token to store
    def save(token)
      # To be overridden by subclass
    end

    # Clear store
    def clear
      # To be overridden by subclass
    end  

  end

end