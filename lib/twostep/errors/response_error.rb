module Twostep
  class ResponseError < APIError
    attr_reader :status

    def initialize(status)
      @status = status
    end
  end
end