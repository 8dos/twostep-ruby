require 'jwt'

module Twostep 
  class Token < Entity

    def is_valid?
      if @attributes[:access_token] and @attributes[:refresh_token] 
        true
      else
        false  
      end
    end
    
    def is_expired?
      begin
        decoded_token = JWT.decode @attributes[:access_token], nil, false
        payload = decoded_token[0]
        exp = payload["exp"].to_i
        if exp < Time.now.to_i
          true # expired
        else 
          false # still valid 
        end  
      rescue JWT::DecodeError
        true # invalid token
      end
    end

  end  
end  
