module Twostep
  class Client < AbstractClient

    def create_user(email, phone, country_code, send_install_link=false)
      data = {
        email: email, 
        phone: phone, 
        country_code: country_code, 
        send_install_link: send_install_link
      }
      resp = post("/v1/users", data)
      user = User.new(resp.data.to_h) 
      user
    end

    def get_user(user_id)
      resp = get("/v1/users/#{user_id}")
      user = User.new(resp.data.to_h) 
      user
    end

    def request_sms(user_id, force=false)
      data = {
        force: force
      }
      resp = post("/v1/users/#{user_id}/sms", data) 
      sms = Sms.new(resp.data.to_h) 
      sms
    end

    def request_call(user_id, force=false)
      data = {
        force: force
      }
      resp = post("/v1/users/#{user_id}/call", data)    
      call = Call.new(resp.data.to_h) 
      call
    end

    def remove_user(user_id)
      resp = delete("/v1/users/#{user_id}")
      user = User.new(resp.data.to_h) 
      user
    end

  end
end