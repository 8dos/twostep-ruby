require "httparty"
require "json"

module Twostep
  class AbstractClient
    include HTTParty

    def initialize(options = {})
      @client_id = options[:client_id]
      @client_secret = options[:client_secret]

      @api_url = options.fetch(:api_url) { "https://api.twostep.io" }   

      @sandbox = options[:sandbox]
      if @sandbox == true 
        @api_url =  "http://localhost:4000" # "https://sandbox-api.twostep.io"
      end

      @token_store = options[:token_store] 

      @timeout = options.fetch(:timeout) { 30 }   
      @user_agent =  "twostep-ruby/#{VERSION}"                   
    end

    def client_id
      unless @client_id
        raise ArgumentError.new("No Client ID provided") 
      end
      @client_id
    end

    def client_secret
      unless @client_secret
        raise ArgumentError.new("No Client secret provided") 
      end
      @client_secret
    end
  
    def get(path)
      request_with_token("GET", path)
    end   

    def post(path, params=nil)
      request_with_token("POST", path, params)
    end   

    def put(path, params=nil)
      request_with_token("PUT", path, params)
    end   

    def delete(path)
      request_with_token("DELETE", path)
    end   

    def request_with_token(method, path, params=nil)
      token = get_token
      request(method, path, params, token)
    end

    def request(method, path, params, token)
      headers = { 
        "User-Agent" => "twostep-ruby/#{VERSION}", 
        "Accept" => "application/json"
      }

      if !token.nil?
        headers["Authorization"] = "Bearer #{token.access_token}"
      end

      begin
        case method
        when "GET"
          resp = self.class.get(path, base_uri: @api_url, timeout: @timeout, :headers => headers)
        when "POST"
          headers["Content-Type"] = "application/json"
          resp = self.class.post(path, base_uri: @api_url, timeout: @timeout, :headers => headers, :body => params.to_json)
        when "PUT"
          headers["Content-Type"] = "application/json"
          resp = self.class.put(path, base_uri: @api_url, timeout: @timeout, :headers => headers, :body => params.to_json)
        when "DELETE"  
          resp = self.class.delete(path, base_uri: @api_url, timeout: @timeout, :headers => headers)
        end  

        if [200,201].include?(resp.code)
          JSON.parse(resp.body, object_class: Entity)   #FIXME: Handle 204 No content
        else  
          begin
            err = JSON.parse(resp.body, object_class: Entity)
            raise ResponseError.new(resp.code), err.message
          rescue JSON::ParserError
            raise ResponseError.new(resp.code), resp.message # "Unexpected server response"
          end
        end  
      rescue Timeout::Error, Net::ReadTimeout => e
        raise APITimeoutError.new(e.message)
      rescue Errno::ECONNREFUSED, Errno::EINVAL, Errno::ECONNRESET, EOFError,
        Net::HTTPBadResponse, Net::HTTPHeaderSyntaxError, Net::ProtocolError,
        HTTParty::Error, SocketError => e
        raise APIConnectionError.new(e.message)
      end
    end

    def get_token()
      token = nil
      # first try get from token store
      token = @token_store.load() if !@token_store.nil?
      if !token.nil? and token.is_valid?
        if token.is_expired?
          return self.refresh_token(token)
        else
          return token
        end
      end    
      new_token()
    end
    
    def new_token()
      payload = {    
        client_id: @client_id,
        client_secret: @client_secret, 
        grant_type: "client_credentials"
      }

      resp = request("POST", "/auth/token", payload, nil)
      token = Token.new(resp.to_h)

      if @token_store != nil
        @token_store.save(token)
      end

      token
    end

    def refresh_token(old_token)
      payload = {    
        client_id: @client_id,
        client_secret: @client_secret, 
        grant_type: "client_credentials",
        refresh_token: old_token.refresh_token,
      }

      resp = request("POST", "/auth/token", payload, nil)
      token = Token.new(resp.to_h)

      if @token_store != nil
        @token_store.save(token)
      end

      token
    end

  end
end  
