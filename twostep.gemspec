require File.expand_path("lib/twostep/version", File.dirname(__FILE__))

Gem::Specification.new do |s|
  s.name = "twostep"
  s.version = Twostep::VERSION
  s.authors = ["Objectia"]
  s.email = ["hello@objectia.com"]
  s.license = "MIT"
  s.homepage = "https://github.com/objectia/twostep-ruby"
  s.description = "Ruby API client for twostep.io"
  s.summary = "This is the Ruby client library for the twostep.io API. To use it you will need a Twostep account. Sign up for free at https://www.twostep.io"
  s.platform = Gem::Platform::RUBY
  s.files = Dir.glob("{lib,test}/**/*") + %w(LICENSE README.md twostep.gemspec)
  s.required_ruby_version = ">= 2.2.0"
  s.add_dependency("jwt", "~> 2.1")
  s.add_dependency("httparty", "~> 0.16")
  s.add_development_dependency("rake", "~> 12.0")
  s.add_development_dependency("minitest", "~> 5.0")
  s.add_development_dependency("simplecov", "~> 0.16")
  s.add_development_dependency("codecov", "~> 0.1.10")
  s.require_path = "lib"
end
