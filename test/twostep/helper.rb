require "simplecov"
SimpleCov.start do
  track_files "/lib/**/*.rb"
  add_filter "/test/"
end

if ENV["CI"] == "true"
  require "codecov"
  SimpleCov.formatter = SimpleCov::Formatter::Codecov
end

require "minitest/autorun"
require "json"
require "twostep"

module TwostepTest

  class TestTokenStore < Twostep::TokenStore
    def initialize
      @token = nil
    end  

      # Load token from store 
    def load
      #puts "loading token from store"
      @token
    end
  
    # Save token to store
    def save(token)
      #puts "saving token to store"
      @token = token    
    end
  end

  class ExpiredTokenStore < Twostep::TokenStore
    def load
      data = {
        "access_token" => "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A",
        "token_type" => "Bearer",
        "scope" => "none",
        "expires_in" => 3600,
        "refresh_token" => "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f"
      }
      str = JSON[data]
      token = JSON.parse(str, object_class: Twostep::Token)
      token
    end
  
    def save(token)
      # nope
    end
  end

  class BadTokenStore < Twostep::TokenStore
    def load
      data = {
        "access_token" => "",
        "token_type" => "Bearer",
        "scope" => "none",
        "expires_in" => 3600,
        "refresh_token" => ""
      }
      str = JSON[data]
      token = JSON.parse(str, object_class: Twostep::Token)
      token
    end
  
    def save(token)
      # nope
    end
  end

  class Test < Minitest::Test
    def client
      @client ||= Twostep::Client.new({
        client_id: "12345",
        client_secret: "67890",
        sandbox: true,
        token_store: TestTokenStore.new,
        timeout: 10,
      })
    end
  end

end