require_relative "./helper"

module TwostepTest

  class TwostepUsersTest < TwostepTest::Test

    def test_with_no_client_id
      assert_raises ArgumentError do
        cli = Twostep::Client.new({
          client_secret: "67890",
          sandbox: true,
        })
        _ = cli.client_id
      end
    end

    def test_with_no_client_secret
      assert_raises ArgumentError do
        cli = Twostep::Client.new({
          client_id: "12345",
          sandbox: true,
        })
        _ = cli.client_secret
      end
    end

    def test_get_user
      user = client.get_user("123456")
      assert_equal "123456", user.id 
      # one more time to test getting of toke from store
      user2 = client.get_user("123456")
      assert_equal "123456", user2.id 
    end

    def test_get_unknown_user
      assert_raises Twostep::ResponseError do
        client.get_user("000000")
      end
    end

    def test_get_user_with_expired_token
      cli = Twostep::Client.new({
        client_id: "12345",
        client_secret: "67890",
        sandbox: true,
        token_store: ExpiredTokenStore.new,
      })
      user = cli.get_user("123456")
      assert_equal "123456", user.id 
    end

    def test_get_user_with_bad_token
      cli = Twostep::Client.new({
        client_id: "12345",
        client_secret: "67890",
        sandbox: true,
        token_store: BadTokenStore.new,
      })
      user = cli.get_user("123456")
      assert_equal "123456", user.id 
    end

    def test_when_server_is_down
      cli = Twostep::Client.new({
        client_id: "12345",
        client_secret: "67890",
        api_url: "http://localhost:4444",
      })
      assert_raises Twostep::APIConnectionError do
        cli.get_user("123456")
      end
    end

    def test_with_fake_server
      cli = Twostep::Client.new({
        client_id: "12345",
        client_secret: "67890",
        api_url: "http://fakeserver:4000",
      })
      assert_raises Twostep::APIConnectionError do
        cli.get_user("123456")
      end
    end

    def test_create_user
      user = client.create_user("jdoe@example.com", "+12125551234", 1)
      refute_nil user 
    end

    def test_create_user_with_invalid_email
      assert_raises Twostep::ResponseError do
        client.create_user("jdoeexample.com", "+12125551234", 1)
      end  
    end

    def test_remove_user
      user = client.remove_user("123456")
      refute_nil user 
    end

    def test_remove_unknown_user
      assert_raises Twostep::ResponseError do
        client.remove_user("000000")
      end
    end

    def test_request_sms
      user = client.request_sms("123456")
      refute_nil user 
    end

    def test_request_sms_unknown_user
      assert_raises Twostep::ResponseError do
        client.request_sms("000000")
      end
    end

    def test_request_call
      user = client.request_call("123456")
      refute_nil user 
    end

    def test_request_call_unknown_user
      assert_raises Twostep::ResponseError do
        client.request_call("000000")
      end
    end

  end
end